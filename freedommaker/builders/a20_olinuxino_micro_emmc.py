# SPDX-License-Identifier: GPL-3.0-or-later
"""
Worker class to build Olimex A20-OLinuXino Micro eMMC image.
"""

from .a20 import A20ImageBuilder


class A20OLinuXinoMicroImageEMMCBuilder(A20ImageBuilder):
    """Image builder for A20 OLinuXino Micro eMMC targets."""
    machine = 'a20-olinuxino-micro-emmc'
    flash_kernel_name = 'Olimex A20-OLinuXino-MICRO-eMMC'
    u_boot_path = \
        'usr/lib/u-boot/A20-OLinuXino_MICRO-eMMC/u-boot-sunxi-with-spl.bin'
