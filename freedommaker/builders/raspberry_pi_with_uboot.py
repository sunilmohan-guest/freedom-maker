# SPDX-License-Identifier: GPL-3.0-or-later
"""
Base worker class to build Raspberry Pi 2 and 3 images.
"""

from .. import library
from .arm import ARMImageBuilder


class RaspberryPiWithUBoot(ARMImageBuilder):
    """Base image builder for Raspberry Pi 2 and 3 targets."""
    include_non_free_firmware = True
    uboot_variant = None
    firmware_filesystem_type = 'vfat'
    firmware_size = '256MiB'
    u_boot_variant = 'rpi'

    def install_boot_loader(self, state):
        """Install the boot loader onto the image."""
        if not self.u_boot_rpi_variant:
            raise NotImplementedError

        firmware_package = 'raspi-firmware'
        script = '''
set -e
set -x
set -o pipefail

apt-get install --no-install-recommends -y dpkg-dev
cd /tmp
apt-get source {firmware_package}
cp {firmware_package}*/boot/* /boot/firmware
rm -rf {firmware_package}*
cd /

# remove unneeded firmware files
rm -f /boot/firmware/fixup_*
rm -f /boot/firmware/start_*

# u-boot setup
apt-get install -y u-boot-rpi
cp /usr/lib/u-boot/{u_boot_rpi_variant}/u-boot.bin /boot/firmware/kernel.img
cp /usr/lib/u-boot/{u_boot_rpi_variant}/u-boot.bin /boot/firmware/kernel7.img
'''.format(firmware_package=firmware_package,
           u_boot_rpi_variant=self.u_boot_rpi_variant)
        library.run_in_chroot(state, ['bash', '-c', script])
