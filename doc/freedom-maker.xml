<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.4//EN" "http://www.docbook.org/xml/4.4/docbookx.dtd">
<!--
# SPDX-License-Identifier: GPL-3.0-or-later
-->
<refentry>
  <refmeta>
    <refentrytitle><application>freedom-maker</application></refentrytitle>
    <manvolnum>1</manvolnum>
    <refmiscinfo class="manual">FreedomBox Manual</refmiscinfo>
    <refmiscinfo class="version">0.32</refmiscinfo>
  </refmeta>

  <refnamediv>
    <refname><application>freedom-maker</application></refname>
    <refpurpose>
      image builder for FreedomBox
    </refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <cmdsynopsis>
      <command>freedom-maker</command>
        <arg> <option>--build-stamp</option></arg>
        <arg> <option>--image-size</option></arg>
        <arg> <option>--build-mirror</option></arg>
        <arg> <option>--mirror</option></arg>
        <arg> <option>--distribution</option></arg>
        <arg> <option>--add-release-component</option></arg>
        <arg> <option>--package</option></arg>
        <arg> <option>--custom-package</option></arg>
        <arg> <option>--enable-backports</option></arg>
        <arg> <option>--disable-backports</option></arg>
        <arg> <option>--build-dir</option></arg>
        <arg> <option>--log-level</option></arg>
        <arg> <option>--hostname</option></arg>
        <arg> <option>--sign</option></arg>
        <arg> <option>--force</option></arg>
        <arg> <option>--build-in-ram</option></arg>
        <arg> <option>--skip-compression</option></arg>
        <arg> <option>--with-build-dep</option></arg>
        <arg> <option>targets</option></arg>
        <arg><option>-h, </option><option>--help</option></arg>
   </cmdsynopsis>
  </refsynopsisdiv>

  <refsect1>
    <title>Description</title>
    <para>
      FreedomBox is a community project to develop, design and promote
      personal servers running free software for private, personal
      communications.  It is a networking appliance designed to allow
      interfacing with the rest of the Internet under conditions of
      protected privacy and data security.  It hosts applications such
      as blog, wiki, website, social network, email, web proxy and a
      Tor relay on a device that can replace a wireless router so that
      data stays with the users.
    </para>
    <para>
      freedom-maker is a tool to build FreedomBox images for various single
      board computers, virtual machines and general purpose computers.
    </para>
  </refsect1>

  <refsect1>
    <title>Options</title>
    <variablelist>
      <varlistentry>
        <term> <option>--build-stamp</option></term>
         <listitem>
          <para>
            Build stamp to use on image file names
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term> <option>--image-size</option></term>
         <listitem>
          <para>
            Size of the image to build
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term> <option>--build-mirror</option></term>
         <listitem>
          <para>
            Debian mirror to use for building
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term> <option>--mirror</option></term>
         <listitem>
           <para>
             Debian mirror to use in built image
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term> <option>--distribution</option></term>
         <listitem>
           <para>
             Debian release to use in built image
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term> <option>--add-release-component</option></term>
         <listitem>
           <para>
             Add an extra Debian release component (other than main)
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term> <option>--package</option></term>
         <listitem>
           <para>
             Install additional packages in the image
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term> <option>--custom-package</option></term>
         <listitem>
           <para>
             Install package from DEB file into the image
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term> <option>--enable-backports</option></term>
         <listitem>
           <para>
             Deprecated: Backports are now enabled for stable images by default
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term> <option>--disable-backports</option></term>
         <listitem>
           <para>
             Disable backports in the image
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term> <option>--build-dir</option></term>
         <listitem>
           <para>
             Directory to build images and create log file
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term> <option>--log-level'</option></term>
         <listitem>
           <para>
             The logging level - choose one of ('critical',
             'error', 'warn', 'info', 'debug'). Default log level
             is 'debug'.
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term> <option>--hostname</option></term>
         <listitem>
          <para>
            Hostname to set inside the built images
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term> <option>--sign</option></term>
         <listitem>
           <para>
             Sign the images with default GPG key after building
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term> <option>--force</option></term>
         <listitem>
           <para>
             Force rebuild of images even when required image exists
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term> <option>--build-in-ram</option></term>
         <listitem>
           <para>
             Build the image in RAM so that it is faster, requires
             free RAM about the size of disk image
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term> <option>--with-build-dep</option></term>
         <listitem>
           <para>
             Include build dependencies in the image
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term> <option>targets</option></term>
        <listitem>
          <para>
            Image targets to build. Choose one or more of a20-olinuxino-lime,
            a20-olinuxino-lime2, a20-olinuxino-micro, amd64, arm64, armhf,
            banana-pro, beaglebone, cubieboard2, cubietruck, i386, lamobo-r1,
            orange-pi-zero, pcduino3, pine64-lts, pine64-plus, qemu-amd64,
            qemu-i386, raspberry2, raspberry3, raspberry3-b-plus, raspberry64,
            vagrant virtualbox-amd64, virtualbox-i386
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsect1>

  <refsect1>
    <title>Examples</title>

    <example>
      <title>Build BeagleBone image</title>
      <synopsis>$ freedom-maker beaglebone</synopsis>
      <para>
        Build a FreedomBox image for the BeagleBone Single Board Computer.
      </para>
    </example>

    <example>
      <title>Build all images</title>
      <synopsis>$ freedommaker a20-olinuxino-lime a20-olinuxino-lime2
      a20-olinuxino-micro amd64 arm64 armhf banana-pro beaglebone cubieboard2
      cubietruck i386 lamobo-r1 orange-pi-zero pcduino3 pine64-lts pine64-plus
      qemu-amd64 qemu-i386 raspberry2 raspberry3 raspberry3-b-plus raspberry64
      vagrant virtualbox-amd64 virtualbox-i386</synopsis>
      <para>
        Build all the available FreedomBox images using freedom-maker.
      </para>
    </example>
  </refsect1>

  <refsect1>
    <title>Bugs</title>
    <para>
      See the <ulink
      url="https://salsa.debian.org/freedombox-team/freedom-maker/-/issues">
      freedom-maker issue tracker</ulink> for a full list of known issues and TODO items.
    </para>
  </refsect1>

  <refsect1>
    <title>Author</title>
    <para>
      <author>
        <firstname>FreedomBox Developers</firstname>
        <contrib>Original author</contrib>
      </author>
    </para>
  </refsect1>
</refentry>
